package com.daddyscode.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class ResourceManager {
	protected SessionFactory sessionFactory;
	
	protected void setup() {
		StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
		Metadata metadata = new MetadataSources(standardServiceRegistry).getMetadataBuilder().build();
		
		try {
			sessionFactory = metadata.getSessionFactoryBuilder().build();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
		}
	}
	
	protected void read() {
		Session session = sessionFactory.openSession();
		
		Projects projects;
	}
	
	protected void exit() {
		sessionFactory.close();
	}

	public static void main(String[] args) {
		
	}

}
