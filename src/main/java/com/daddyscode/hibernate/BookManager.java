package com.daddyscode.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class BookManager {
	protected SessionFactory sessionFactory;
	
	protected void setup() {
		StandardServiceRegistry standardServiceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();  
        Metadata metadata = new MetadataSources(standardServiceRegistry).getMetadataBuilder().build();
        
        try {
        	sessionFactory = metadata.getSessionFactoryBuilder().build();
		} catch (Exception e) {
			e.printStackTrace();
			StandardServiceRegistryBuilder.destroy(standardServiceRegistry);
		}
	}
	
	protected void exit() {
		sessionFactory.close();
	}
	
	protected void create() {
		Book book = new Book();
		book.setTitle("Book 2");
		book.setAuthor("Author 2");
		book.setPrice(500);
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(book);
		session.getTransaction().commit();
		session.close();
	}
	
	protected void read() {
		Session session = sessionFactory.openSession();
		
		Book book = session.get(Book.class, 2l);
		
		System.out.println("Title: " + book.getTitle());
		System.out.println("Author: " + book.getAuthor());
		System.out.println("Price: " + book.getPrice());
		
		session.close();
	}
	
	protected void update() {
		Book book = new Book();
		book.setId(1);
		book.setTitle("Norse Mythology");
		book.setAuthor("Neil Gaiman");
		book.setPrice(599);
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(book);
		session.getTransaction().commit();
		session.close();
	}
	
	protected void delete() {
		Book book = new Book();
		book.setId(1);
		
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(book);
		session.getTransaction().commit();
		session.close();
		
	}
	
	public static void main(String[] args) {
		BookManager manager = new BookManager();
	    manager.setup();
//	    manager.create();
//	    manager.read();
//	    manager.delete();
	    manager.exit();
        
	}
}
