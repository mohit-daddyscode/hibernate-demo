package com.daddyscode.hibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class UserDao {

	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("UsersDB");
		EntityManager entityManager = factory.createEntityManager();
		
		entityManager.getTransaction().begin();
		
		/*
		 * Users newUser = new Users(); newUser.setFullname("User 2");
		 * newUser.setEmail("user2@user.com"); newUser.setPassword("user2pwd");
		 * 
		 * entityManager.persist(newUser);
		 */
		
		/*
		 * Users existingUser = new Users(); existingUser.setId(2);
		 * existingUser.setFullname("User Two");
		 * existingUser.setEmail("user2@user.com");
		 * existingUser.setPassword("user2pwd");
		 * 
		 * entityManager.merge(existingUser);
		 */
		
		/*
		 * Users users = entityManager.find(Users.class, 1);
		 * System.out.println(users.getFullname());
		 */
		
		String sql = "SELECT u FROM Users u where u.fullname = 'User Two'";
		Query query = entityManager.createQuery(sql);
		Users users = (Users) query.getSingleResult();
		
		System.out.println(users.getFullname());
		System.out.println(users.getEmail());
		
		entityManager.getTransaction().commit();
		entityManager.close();
		factory.close();
	}
}
